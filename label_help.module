<?php

/**
 * @file
 * Primary module hooks for the Label Help module.
 */

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element;
use Drupal\Core\Site\Settings;

/**
 * @file
 * Label Help module.
 *
 * Adds an additional textfield for all Field API field types to appear
 * between the field's label and the field input itself on Drupal forms.
 */

/**
 * Implements hook_theme().
 */
function label_help_theme() {
  return [
    'label_help' => [
      'variables' => [
        'content' => NULL,
        'attributes' => [],
      ],
    ],
    'label_help__seven' => [
      'template'  => 'label-help--seven',
      'base hook' => 'label-help',
    ],
    'label_help__claro' => [
      'template'  => 'label-help--claro',
      'base hook' => 'label-help',
    ],
    'label_help__gin' => [
      'template'  => 'label-help--gin',
      'base hook' => 'label-help',
    ],
  ];
}

/**
 * Implements hook_form_alter().
 */
function label_help_form_alter(&$form, &$form_state, $form_id) {
  $form['#process'][] = 'label_help_process_form';
}

/**
 * Custom process callback for modifying form elements with Label Help.
 */
function label_help_process_form($element, FormStateInterface $form_state, &$form) {
  $debug = Settings::get('label_help_debug', FALSE);
  $debug_dump = Settings::get('label_help_debug_dump', FALSE);
  $one_time_debug_warning = &drupal_static(__FUNCTION__);
  $children = array_intersect_key($form, array_flip(Element::children($form)));
  foreach ($children as $key => $item) {
    $use_case = 0;
    $content = NULL;
    $fallback_use_case = FALSE;

    // There are two possible ways to add text content for Label Help:
    // Option 1) via code, using a custom #label_help Form API property.
    if (!empty($item['#label_help'])) {
      $content = $item['#label_help'];
    }
    // Option 2) via the Field UI module in the Drupal web interface.
    else {
      $form_object = $form_state->getFormObject();
      if (!method_exists($form_object, 'getEntity')) {
        return $element;
      }
      $method = new ReflectionMethod($form_object, 'getEntity');
      if (!$method->isPublic()) {
        return $element;
      }
      $form_entity = $form_object->getEntity();
      if (!method_exists($form_entity, 'getFieldDefinition')) {
        return $element;
      }
      $method = new ReflectionMethod($form_entity, 'getFieldDefinition');
      if (!$method->isPublic()) {
        return $element;
      }
      $field = $form_entity->getFieldDefinition($key);
      if ($field && method_exists($field, 'getThirdPartySetting')) {
        $content = $field->getThirdPartySetting('label_help', 'label_help_description');
      }
    }
    if (is_null($content) || strlen($content) === 0) {
      continue;
    }

    _label_help_attach_styles($form);

    // Most Drupal 8 entity edit forms have fields of type 'container' with
    // an inner widget where the form element itself is attached. Attempt to
    // leverage #label_suffix where possible and fallback to alternate
    // solutions for widget types like details and fieldsets that do not
    // support #label_suffix.
    if (isset($item['#type']) && $item['#type'] == 'container') {

      // Special case for multi-value fields, which lack #label_suffix
      // support, appends the help text straight to the field title so it
      // appears in the table header, instead of inside the draggable row.
      if (!empty($item['widget']['#cardinality_multiple'])) {
        if (!empty($form[$key]['widget']['#title'])) {
          $use_case = 1;
          $element = &$form[$key]['widget'];
          _label_help_append_title($element, $content, $use_case);
        }
        elseif (!empty($form[$key]['widget']['title'])) {
          $use_case = 2;
          $form[$key]['widget']['title']['#attributes']['class'][] = 'label';
          $form[$key]['widget']['title']['#suffix'] = _label_help_attach_message($content, '#suffix', $use_case);
        }
      }

      // Special case for containers whose first element is to be rendered as
      // a fieldset either via #type (eg, Link fields) or #theme_wrappers (eg,
      // Date fields). Drupal's fieldset.html.twig do not support the
      // #label_prefix, so use #field_prefix instead, but be careful to not
      // overwrite content when the #field_prefix is already defined.
      elseif (
        (
          !empty($item['widget'][0]['#type']) &&
          (
            $item['widget'][0]['#type'] == 'fieldset' ||
            $item['widget'][0]['#type'] == 'checkboxes' ||
            $item['widget'][0]['#type'] == 'radios'
          )
        ) || (
          !empty($item['widget'][0]['#theme_wrappers']) &&
          $item['widget'][0]['#theme_wrappers'][0] == 'fieldset'
        )
      ) {
        $use_case = 3;
        $element = &$form[$key]['widget'][0];
        _label_help_prepend_field_prefix($element, $content, $use_case);
      }

      // Special case for media, checkboxes and radios where Drupal's
      // fieldset.html.twig does not support the #label_prefix, so use
      // #field_prefix instead, but be careful to not overwrite content
      // when the #field_prefix is already defined.
      elseif (
        (
          !empty($item['widget']['#type']) &&
          !empty($item['widget']['#title']) &&
          (
            $item['widget']['#type'] == 'fieldset' ||
            $item['widget']['#type'] == 'checkboxes' ||
            $item['widget']['#type'] == 'radios'
          )
        )
      ) {
        $use_case = 4;
        $element = &$form[$key]['widget'];
        _label_help_prepend_field_prefix($element, $content, $use_case);
      }

      // Single on/off checkbox.
      elseif (
        isset($item['widget']['value']) &&
        $item['widget']['value']['#type'] == 'checkbox'
      ) {
        $use_case = 5;
        $element = &$form[$key]['widget']['value'];
        _label_help_prepend_field_prefix($element, $content, $use_case);
      }

      // Special case for containers with a details widget, specified
      // either via #type or #theme_wrappers (eg Entity Browser or Address
      // fields). Drupal's details.html.twig does not support #label_prefix,
      // so we use #description instead, but be careful to not overwrite
      // content when the #description is already defined.
      elseif (
        (
          !empty($item['widget']['#type']) &&
          $item['widget']['#type'] == 'details'
        ) || (
          !empty($item['widget']['#theme_wrappers']) &&
          $item['widget']['#theme_wrappers'][0] == 'details'
        )
      ) {
        $use_case = 6;
        $element = &$form[$key]['widget'];
        _label_help_prepend_description($element, $content, $use_case);
      }

      // Special case for containers whose first element is rendered as a
      // details widget, specified either via #type or #theme_wrappers (eg
      // Address fields). Drupal's details.html.twig does not support
      // #label_prefix, so we use #description instead, but be careful to not
      // overwrite content when the #description is already defined.
      elseif (
        (
          !empty($item['widget'][0]['#type']) &&
          $item['widget'][0]['#type'] == 'details'
        ) || (
          !empty($item['widget'][0]['#theme_wrappers'])
          && $item['widget'][0]['#theme_wrappers'][0] == 'details'
        )
      ) {
        $use_case = 7;
        $element = &$form[$key]['widget'][0];
        _label_help_prepend_description($element, $content, $use_case);
      }

      // Special case for datetime form elements which do not properly display
      // the label help using #label_suffix, #field_prefix, nor take into
      // account #description_display.  Therefore we use a hack solution
      // append the message to the field #title.
      elseif (
        isset($item['widget']['#theme']) &&
        $item['widget']['#theme'] == 'field_multiple_value_form' &&
        isset($item['widget'][0]['value']['#type']) &&
        $item['widget'][0]['value']['#type'] == 'datetime'
      ) {
        $use_case = 8;
        $element = &$form[$key]['widget'][0]['value'];
        _label_help_append_title($element, $content, $use_case);
      }

      elseif (isset($item['widget'][0]['value'])) {
        $use_case = 9;
        $element = &$form[$key]['widget'][0]['value'];
        _label_help_prepend_field_prefix($element, $content, $use_case);
      }

      // Special case for link fields when 'Allow link text' option is disabled,
      // the widget does not have any type and the field is rendered as a
      // textfield with its field value in the 'uri' key. Therefore, we place
      // the message in the #field_prefix.
      elseif (isset($item['widget'][0]['uri'])) {
        $use_case = 10;
        $element = &$form[$key]['widget'][0]['uri'];
        _label_help_prepend_field_prefix($element, $content, $use_case);
      }

      // Format used by Client-side Hierarchical Select module.
      elseif (isset($item['widget'][0]['target_id']['#title'])) {
        $use_case = 11;
        $element = &$form[$key]['widget'][0]['target_id'];
        _label_help_append_label_suffix($element, $content, $use_case);
      }

      elseif (isset($item['widget'][0]['#title'])) {
        $use_case = 12;
        $element = &$form[$key]['widget'][0];
        _label_help_append_label_suffix($element, $content, $use_case);
      }

      // Eg, Select lists.
      elseif (isset($item['widget']['#title'])) {
        $use_case = 13;
        $element = &$form[$key]['widget'];
        _label_help_append_label_suffix($element, $content, $use_case);
      }

      elseif (isset($item['widget']['target_id']['#title'])) {
        $use_case = 14;
        $element = &$form[$key]['widget']['target_id'];
        _label_help_append_label_suffix($element, $content, $use_case);
      }

      else {
        $use_case = 15;
        $fallback_use_case = TRUE;
      }
    }

    // Custom fields may not be defined with a container or a widget wrapper.
    // To keep things simple, place the label in the field prefix.
    elseif (isset($item['#type']) && empty($item['widget'])) {
      $use_case = 16;
      $element = &$form[$key];
      _label_help_append_label_suffix($element, $content, $use_case);
    }
    else {
      $use_case = 17;
      $fallback_use_case = TRUE;
    }

    // Add a fallback use case to place message in the element label.
    if ($fallback_use_case) {
      $element = &$form[$key]['widget'];
      _label_help_append_label_suffix($element, $content, $use_case);
      if ($debug) {
        $form["debug_$key"] = [
          '#markup' => "Label Help Debug: Fallback placement: $key (use case $use_case)",
          '#weight' => (int) $form[$key]['#weight'] - 1,
        ];
      }
    }

    if ($use_case && $debug_dump) {
      if (empty($one_time_debug_warning)) {
        dump("Note: the following variable dump(s) may prevent proper page rendering. This is expected behavior.");
        $one_time_debug_warning = TRUE;
      }
      if ($fallback_use_case) {
        dump("Label Help Debug: $key (use case $use_case) UNKNOWN FIELD TYPE!");
      }
      else {
        dump("Label Help Debug: $key (use case $use_case)");
      }
      dump($item);
    }
  }

  return $form;
}

/**
 * Implements hook_form_FORM_ID_alter().
 */
function label_help_form_field_config_edit_form_alter(&$form, &$form_state, $form_id) {
  $fieldConfig = $form_state->getFormObject()->getEntity();
  // Add settings for file upload widgets.
  $form['settings']['label_help_description'] = [
    '#type' => 'textarea',
    '#rows' => 2,
    '#title' => t('Label help message'),
    '#default_value' => $fieldConfig->getThirdPartySetting('label_help', 'label_help_description'),
    '#description' => t('Help text to insert below the label and above the input form element.'),
  ];
  $form['#entity_builders'][] = 'label_help_form_field_config_edit_form_builder';
}

/**
 * Entity builder for the menu configuration entity.
 */
function label_help_form_field_config_edit_form_builder($entity_type, $field, &$form, FormStateInterface $form_state) {
  if ($form_state->getValue(['settings', 'label_help_description'])) {
    $field->setThirdPartySetting('label_help', 'label_help_description', $form_state->getValue([
      'settings',
      'label_help_description',
    ]));
    return;
  }
  $field->unsetThirdPartySetting('label_help', 'label_help_description');
}

/**
 * Implements hook_preprocess_HOOK().
 */
function label_help_preprocess_form_element(&$variables) {
  if (!isset($variables['element']['#name'])) {
    return;
  }
  // Set the label suffix/prefix.
  $element = &$variables['element'];
  if (!empty($variables['label'])) {
    if (!empty($element['#label_prefix'])) {
      $variables['label']['#prefix'] = $element['#label_prefix'];
    }
    if (!empty($element['#label_suffix'])) {
      $variables['label']['#suffix'] = $element['#label_suffix'];
    }
  }
}

/**
 * Implements hook_theme_suggestions_HOOK() for label-help.html.twig.
 */
function label_help_theme_suggestions_label_help(array $variables) {
  $suggestions = [];
  foreach (_label_help_get_theme_stack() as $theme_name) {
    $suggestions[] = 'label_help__' . $theme_name;
  }
  if (!empty($suggestions)) {
    return $suggestions;
  }
}

/**
 * Appends a message to the title attribute of an element.
 *
 * @param array $element
 *   The render array element to modify.
 * @param string $content
 *   The message content to append to the title.
 * @param int $use_case
 *   An optional parameter to specify the use case for the message.
 */
function _label_help_append_title(&$element, $content, $use_case = 0) {
  // Add 'label' class to the title attributes.
  $element['#title_attributes']['class'][] = 'label';
  // Append the message to the element's title.
  $element['#title'] = $element['#title'] . _label_help_attach_message($content, '#title on element', $use_case);
}

/**
 * Appends a message to the label suffix of an element.
 *
 * @param array $element
 *   The render array element to modify.
 * @param string $content
 *   The message content to append to the label suffix.
 * @param int $use_case
 *   An optional parameter to specify the use case for the message.
 */
function _label_help_append_label_suffix(&$element, $content, $use_case = 0) {
  // Check if the label suffix is not empty.
  if (!empty($element['#label_suffix'])) {
    // Append the message to the existing label suffix.
    $element['#label_suffix'] = _label_help_attach_message($content, '#label_suffix', $use_case) . $element['#label_suffix'];
  }
  // If the label suffix is empty, set it with the message.
  $element['#label_suffix'] = _label_help_attach_message($content, '#label_suffix', $use_case);
}

/**
 * Prepends a message to the description of an element.
 *
 * @param array $element
 *   The render array element to modify.
 * @param string $content
 *   The message content to prepend to the description.
 * @param int $use_case
 *   An optional parameter to specify the use case for the message.
 */
function _label_help_prepend_description(&$element, $content, $use_case = 0) {
  // Convert existing description to a render array if it is not already.
  if (!empty($element['#description']) && !is_array($element['#description'])) {
    $element['#description'] = [
      'existing' => [
        '#markup' => $element['#description'],
        '#weight' => 0,
      ],
    ];
  }
  if (!is_array($element['#description'])) {
    $element['#description'] = [];
  }
  // Prepend the message to the description render array.
  $element['#description']['label_help'] = [
    '#markup' => _label_help_attach_message($content, '#description', $use_case),
    '#weight' => -10,
  ];
}

/**
 * Prepends a message to the field prefix of an element.
 *
 * @param array $element
 *   The render array element to modify.
 * @param string $content
 *   The message content to prepend to the field prefix.
 * @param int $use_case
 *   An optional parameter to specify the use case for the message.
 */
function _label_help_prepend_field_prefix(&$element, $content, $use_case = 0) {
  // Convert existing field prefix to a render array if it is not already.
  if (!empty($element['#field_prefix']) && !is_array($element['#field_prefix'])) {
    $element['#field_prefix'] = [
      'existing' => [
        '#markup' => $element['#field_prefix'],
        '#weight' => 0,
      ],
    ];
  }
  // Prepend the message to the field prefix render array.
  $element['#field_prefix']['label_help'] = [
    '#markup' => _label_help_attach_message($content, '#field_prefix', $use_case),
    '#weight' => -10,
  ];
}

/**
 * Renders the message text with additional debug text, when debugging is on.
 *
 * @param string $content
 *   The content to render.
 * @param string $placement
 *   The Form API property name where the label is ultimately rendered. E.g.
 *   '#title', '#suffix', '#label_suffix', '#field_prefix', '#description'.
 * @param int $use_case
 *   The integer indicating which widget type was identified by the Label
 *   Help placement logic in label_help_form_alter().
 *
 * @return string
 *   The rendered content.
 */
function _label_help_attach_message($content, $placement, $use_case = 0) {
  // Provide themeable markup for the help text.
  $renderer = \Drupal::service('renderer');
  $debug = Settings::get('label_help_debug', FALSE);
  $status = ($use_case !== 20) ? '✅' : '❌';
  if ($debug) {
    $content = "$status $content $placement (use case $use_case)";
  }
  $element = [
    '#theme' => 'label_help',
    '#content' => [
      '#markup' => $content,
    ],
  ];
  return $renderer->renderRoot($element);
}

/**
 * Attaches the appropriate theme styles to the renderable element.
 *
 * @param array $element
 *   The render element (for example, $form or $variables) to attach the
 *   library.
 */
function _label_help_attach_styles(array &$element) {
  $theme_stack = _label_help_get_theme_stack();
  if (in_array('seven', $theme_stack)) {
    $element['#attached']['library'][] = 'label_help/seven';
  }
  if (in_array('claro', $theme_stack)) {
    $element['#attached']['library'][] = 'label_help/claro';
  }
  if (in_array('gin', $theme_stack)) {
    $element['#attached']['library'][] = 'label_help/gin';
  }
}

/**
 * Retrieves the list of theme machine names in the current theme stack.
 *
 * The theme stack is sorted by ancestor themes first, and then active themes.
 *
 * @return array
 *   The list of theme machine names in the current theme stack.
 */
function _label_help_get_theme_stack() {
  $themes = [];
  $active_theme = \Drupal::theme()
    ->getActiveTheme()->getName();
  $active_theme_info = \Drupal::service('theme_handler')
    ->listInfo()[$active_theme];
  $base_themes = $active_theme_info->base_themes ?? '';
  if (!empty($active_theme)) {
    $themes = [$active_theme];
  }
  if (is_array($base_themes)) {
    $themes = array_merge(array_reverse(array_keys($base_themes)), $themes);
  }
  return $themes;
}
